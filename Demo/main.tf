provider "aws" {
    region = "us-east-1"
}

#specify the EC2 details#
resource "aws_instance" "example"  {
    ami = "ami-053b0d53c279acc90"
    instance_type = "t2.micro"

tags= {
	Name="statelock"
}
}
